﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.WindowsAPICodePack.Taskbar;

namespace glazbaCopy
{
    public partial class PlaylistaKopiranje : Form
    {
        private string file = "";
        private string folder = "";
        private string CommonPrefix = string.Empty;

        private delegate void delegatAsync(ProgressBar bar);

        private delegate void async2(ProgressBar bar);

        private delegate void pbMaxDel(int max);

        private delegate void pbIncDel();

        private delegate void enaKop();

        private TaskbarManager tbManager = null;
        private bool taksbarProgressSupported = false;

        public PlaylistaKopiranje()
        {
            InitializeComponent();
            napredak.Minimum = 0;
            napredak.Maximum = 1;
            napredak.Step = 1;
        }

        //
        // ukloniti X gumb jer se ne smije zatvarati putem njega
        // sruši se jer djeca ne završe kada treba
        //
        private const int CP_NOCLOSE_BUTTON = 0x200;

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        //otvori playlistu
        private void button1_Click(object sender, EventArgs e)
        {
            fileDialog.Title = "Open playlist file";
            fileDialog.Filter = "playlist|*.m3u8;*.m3u";
            fileDialog.Multiselect = false;
            DialogResult result = fileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.file = fileDialog.FileName;
                this.tbUlaz.Text = this.file;
            }
        }

        //otvori odredište
        private void btnDest_Click(object sender, EventArgs e)
        {
            DialogResult result = this.folderDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.folder = Path.Combine(folderDialog.SelectedPath, "Music");
                this.tbIzlaz.Text = this.folder;
            }
        }

        private void pbInc()
        {
            try
            {
                if (this.InvokeRequired)
                {
                    this.Invoke(new pbIncDel(pbInc));
                }
                else
                {
                    this.napredak.Increment(1);
                }
            }
            catch (Exception)
            {
            }
        }

        private void pbMax(int max)
        {
            try
            {
                if (this.InvokeRequired)
                {
                    this.Invoke(new pbMaxDel(pbMax), max);
                }
                else
                {
                    this.napredak.Maximum = max;
                    this.napredak.Minimum = 0;
                    this.napredak.Value = 0;
                }
            }
            catch (Exception)
            {
            }
        }

        private void eKop()
        {
            try
            {
                if (this.InvokeRequired)
                {
                    this.Invoke(new enaKop(eKop));
                }
                else
                {
                    this.btnKopiraj.Enabled = true;
                }
            }
            catch (Exception)
            {
            }
        }

        //ASINKRONO KOPIRANJE!
        private void aCp(ProgressBar bar)
        {
            try
            {
                //da se pojavljuje i u taskbaru progressBar
                if (TaskbarManager.IsPlatformSupported)
                {
                    taksbarProgressSupported = true;
                    tbManager = TaskbarManager.Instance;
                    tbManager.SetProgressState(TaskbarProgressBarState.Normal);
                }
            }
            catch (Exception)
            {
            }
            if (validate())
            {
                var preskoceni = new StringBuilder();

                try
                {
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }

                    //bar.Value = 0;//**************************
                    string sourcePath;
                    System.IO.StreamReader fr = new System.IO.StreamReader(this.file, Encoding.Default);
                    sourcePath = fr.ReadLine();
                    if (sourcePath != "#EXTM3U")
                    {
                        MessageBox.Show("Not valid input file!");
                        fr.Close();
                        enaKop btn = new enaKop(eKop);
                        btn.Invoke();
                        return;
                    }

                    fr.Close();

                    //****************************
                    pbMaxDel max = new pbMaxDel(pbMax);
                    int NlinMax = (File.ReadLines(this.file).Count() - 1) / 2;
                    max.Invoke(NlinMax);
                    if (taksbarProgressSupported)
                    {
                        tbManager.SetProgressValue(0, NlinMax);
                    }

                    if (cbFolders.Checked)
                    {
                        FindCommonPrefix();
                    }
                    int count = 0;
                    fr = new System.IO.StreamReader(this.file, Encoding.Default);
                    sourcePath = fr.ReadLine();
                    while ((sourcePath = fr.ReadLine()) != null)
                    {
                        count++;
                        sourcePath = fr.ReadLine();  //jer mora preskočti parne

                        //kopiranje redom i napredak zabilježen
                        string sourceFileName = Path.GetFileName(sourcePath);
                        string destPath = Path.Combine(this.folder, sourceFileName);

                        if (cbFolders.Checked)
                        {
                            destPath = GetFolderDestination(sourcePath);
                        }

                        if (!File.Exists(destPath))
                        {
                            if (File.Exists(sourcePath))
                            {
                                File.Copy(sourcePath, destPath, false);
                            }
                            else
                            {
                                preskoceni.AppendLine(sourceFileName);
                            }
                        }

                        if (taksbarProgressSupported)
                        {
                            //u taskbaru zabilježi napredak (trenutni, maksimalni)
                            this.tbManager.SetProgressValue(count, NlinMax);
                        }

                        // bar.Increment(1);//*******************************
                        pbIncDel inc = new pbIncDel(pbInc);
                        inc.Invoke();
                    }

                    fr.Close();
                    MessageBox.Show("Finished!");
                }
                catch (IOException greska)
                {
                    MessageBox.Show("Error:\n\n" + greska.ToString());
                }
                finally
                {
                    enaKop btn = new enaKop(eKop);
                    btn.Invoke();
                    if (preskoceni.Length > 0)
                    {
                        var preskoceniFiles = preskoceni.ToString();
                        MessageBox.Show("Not found:\n\n" + preskoceniFiles, "Skipped!");
                        File.WriteAllText(Path.Combine(folder, "!A_Skipped.txt"), "Not found:" + Environment.NewLine + preskoceniFiles);
                    }
                    taksbarProgressSupported = false;
                    file = string.Empty;
                    folder = string.Empty;
                }
            }
            else
            {
                enaKop btn = new enaKop(eKop);
                btn.Invoke();
            }
        }

        private void FindCommonPrefix()
        {
            string currentPrefix = string.Empty;
            using (StreamReader fr = new System.IO.StreamReader(this.file, Encoding.Default))
            {
                string line = fr.ReadLine();
                bool end = false;
                while ((line = fr.ReadLine()) != null && !end)
                {
                    // every second line is interesting
                    line = fr.ReadLine();

                    if (string.IsNullOrEmpty(currentPrefix))
                    {
                        currentPrefix = line.Substring(0, line.LastIndexOf('\\'));
                    }
                    else
                    {
                        while (!line.StartsWith(currentPrefix) && !string.IsNullOrEmpty(currentPrefix))
                        {
                            if (currentPrefix.Contains('\\'))
                            {
                                currentPrefix = currentPrefix.Substring(0, currentPrefix.LastIndexOf('\\'));
                            }
                            else
                            {
                                currentPrefix = string.Empty;
                            }
                        }

                        if (string.IsNullOrEmpty(currentPrefix))
                        {
                            end = true;
                        }
                    }
                }
            }

            Invoke((MethodInvoker)delegate { tbCommonPrefix.Text = currentPrefix; });
            this.CommonPrefix = currentPrefix;
        }

        private string GetFolderDestination(string sourcePath)
        {
            sourcePath = sourcePath.Replace(this.CommonPrefix, string.Empty).Trim('\\');

            string destFolder = sourcePath.Substring(0, sourcePath.LastIndexOf('\\'));

            string destFolderFullPath = Path.Combine(this.folder, destFolder);

            if (!Directory.Exists(destFolderFullPath))
            {
                Directory.CreateDirectory(destFolderFullPath);
            }

            string sourceFileName = Path.GetFileName(sourcePath);
            string dest = Path.Combine(destFolderFullPath, sourceFileName);

            return dest;
        }

        // kopiraj pozivajući putem delegata asinkrono kopiranje
        private void kopiraj_Click(object sender, EventArgs e)
        {
            this.btnKopiraj.Enabled = false;
            delegatAsync kop = new delegatAsync(aCp);
            kop.BeginInvoke(this.napredak, null, null);
        }

        private bool validate()
        {
            if (this.file == "")
            {
                if (tbUlaz.Text == "")
                {
                    MessageBox.Show("Pick playlist!");
                    return false;
                }
                else
                {
                    this.file = tbUlaz.Text;
                }
            }
            if (this.folder == "")
            {
                if (tbIzlaz.Text == "")
                {
                    MessageBox.Show("Pick destination folder!");
                    return false;
                }
                else
                {
                    this.folder = tbIzlaz.Text;
                }
            }
            return true;
        }

        private void btnOdustani_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
